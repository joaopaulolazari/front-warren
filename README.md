# Front Warren
Desenvolvi esse projeto me atualizando sobre o framework [VueJs](https://vuejs.org/) tendo dito que estava mais atualizado sobre o framework React e ReactNative.
Mas com meu conhecimento prévio e algumas atualizações sobre o VueJs eu consegui desenvolver toda a aplicação usando a tecnologia utilizada em sua stack atual.
Tecnologias utilizadas:

* [VueJs](https://vuejs.org/)
* [Vuex](https://vuex.vuejs.org/)
* [Vuetify](https://vuetifyjs.com/en/)

Tomei a decisão de utilizar um framework para UI pois gosto muito de desenvolver mas nem sempre tenho muito bom gosto com a interface de usuário rsrs. Então utilizei uma ferramenta para deixar a interface um pouco agradável :)

Além disso, construi o projeto usando o conceito de Mobile First, ou seja, ele funciona em uma tela de PC, mas pessoalmente prefiri o layout aplicado em uma resolução de mobile (recomendo o padrão do galaxy s5 do devtools).

Utilizo o artifício de colocar o token dentro da aplicação para utilizar junto a API para autorização e pegar o user_id dentro desse token. Coloco ele como uma variável global do Vue e reutilizo nas chamadas (`./src/main.js`).

Na implementação junto ao Vuex Store eu evito todas as vezes que entrar na tela de Saldo onerar uma chamada à API se não tiver sido feito uma transação que altere esse  saldo.

### Desenvolvimento
```sh
npm install
npm run serve
```

### Testes
Implementei alguns testes para garantir o básico do front, pois também tem importancia os testes de front e não somente de backend.
```sh
npm run test:unit
```

### Melhorias e críticas ao desenvolvimento
Algumas melhorias ou críticas:

* Trazer o token de autenticação para dentro da Store do Vuex e não fixado na aplicação.
* Também usar a store na rota de Extrato, para não "gastar" com uma requisição desnecessária.
* Implementar testes automatizados para poder testar melhor as iterações do usuário com a aplicação.
* Entender o porque que quando acesso primeiramente uma rota sem ser a base ("/") dá um erro com o input currency que escolhi usar.