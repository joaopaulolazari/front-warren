import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import TransactionForm from '@/components/TransactionForm.vue';
import Deposit from '@/views/Deposit.vue';

describe('Views :: Deposit', () => {
  let localVue;

  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly layout of component and attr', () => {
    const wrapper = shallowMount(Deposit, { localVue });
    const transactionForm = wrapper.find(TransactionForm);
    expect(transactionForm.exists()).toBeTruthy();
    expect(transactionForm.attributes().title).toBe('depósito');
    expect(transactionForm.attributes().type).toBe('deposit');
  });
});
