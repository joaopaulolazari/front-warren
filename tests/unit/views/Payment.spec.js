import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import TransactionForm from '@/components/TransactionForm.vue';
import Payment from '@/views/Payment.vue';

describe('Views :: Payment', () => {
  let localVue;

  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly layout of component and attr', () => {
    const wrapper = shallowMount(Payment, { localVue });
    const transactionForm = wrapper.find(TransactionForm);
    expect(transactionForm.exists()).toBeTruthy();
    expect(transactionForm.attributes().title).toBe('pagamento');
    expect(transactionForm.attributes().type).toBe('payment');
  });
});
