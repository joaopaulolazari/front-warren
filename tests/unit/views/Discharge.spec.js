import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import TransactionForm from '@/components/TransactionForm.vue';
import Discharge from '@/views/Discharge.vue';

describe('Views :: Discharge', () => {
  let localVue;

  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly layout of component and attr', () => {
    const wrapper = shallowMount(Discharge, { localVue });
    const transactionForm = wrapper.find(TransactionForm);
    expect(transactionForm.exists()).toBeTruthy();
    expect(transactionForm.attributes().title).toBe('resgate');
    expect(transactionForm.attributes().type).toBe('discharge');
  });
});
