import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import Extract from '@/views/Extract.vue';
import '@/plugins/filters';

describe('Views :: Extract', () => {
  let localVue;

  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly layout of component without transactions', () => {
    global.fetch = jest.fn(() => Promise.resolve({ json: () => Promise.resolve([]) }));
    const wrapper = shallowMount(Extract, { localVue });

    const titleHeader = wrapper.find('h3');
    expect(titleHeader.text()).toBe('Extrato');
  });

  it('renders correctly layout of component with transactions', async () => {
    global.fetch = jest.fn(() => Promise.resolve({ json: () => Promise.resolve([]) }));

    const wrapper = shallowMount(Extract, { localVue });
    wrapper.setData({
      transactions: [{
        type: 'deposit',
        created_at: '2021-04-30T17:33:40.480Z',
        value: 123.45,
      }],
    });

    await wrapper.vm.$nextTick();

    const titleHeader = wrapper.find('h3');
    expect(titleHeader.text()).toBe('Extrato');

    const rows = wrapper.findAll({ name: 'v-row' });
    expect(rows).toHaveLength(1);

    const cols = rows.at(0).findAll({ name: 'v-col' });
    expect(cols.at(0).text()).toBe('Depósito');
    expect(cols.at(1).text()).toBe('30/3/2021 14:33');
    expect(cols.at(2).text()).toBe('R$ 123,45');
  });

  it('renders 3 types of transactions', async () => {
    global.fetch = jest.fn(() => Promise.resolve({ json: () => Promise.resolve([]) }));

    const wrapper = shallowMount(Extract, { localVue });
    wrapper.setData({
      transactions: [
        {
          type: 'payment',
          created_at: '2021-04-30T17:33:40.480Z',
          value: 10.00,
        },
        {
          type: 'discharge',
          created_at: '2021-04-29T17:33:40.480Z',
          value: 100.45,
        },

        {
          type: 'deposit',
          created_at: '2021-04-28T17:33:40.480Z',
          value: 123.45,
        },
      ],
    });

    await wrapper.vm.$nextTick();

    const titleHeader = wrapper.find('h3');
    expect(titleHeader.text()).toBe('Extrato');

    const rows = wrapper.findAll({ name: 'v-row' });
    expect(rows).toHaveLength(3);

    const colsPayment = rows.at(0).findAll({ name: 'v-col' });
    expect(colsPayment.at(0).text()).toBe('Pagamento');
    expect(colsPayment.at(1).text()).toBe('30/3/2021 14:33');
    expect(colsPayment.at(2).text()).toBe('R$ 10,00');

    const colsDischarge = rows.at(1).findAll({ name: 'v-col' });
    expect(colsDischarge.at(0).text()).toBe('Resgate');
    expect(colsDischarge.at(1).text()).toBe('29/3/2021 14:33');
    expect(colsDischarge.at(2).text()).toBe('R$ 100,45');

    const colsDeposit = rows.at(2).findAll({ name: 'v-col' });
    expect(colsDeposit.at(0).text()).toBe('Depósito');
    expect(colsDeposit.at(1).text()).toBe('28/3/2021 14:33');
    expect(colsDeposit.at(2).text()).toBe('R$ 123,45');
  });
});
