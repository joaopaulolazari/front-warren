import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import Balance from '@/views/Balance.vue';
import '@/plugins/filters';

describe('Views :: Balance', () => {
  let localVue;

  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
    localVue.use(Vuex);
  });

  it('renders correctly layout of component', () => {
    const getters = {
      balancePrice: () => 0,
      updateBalance: () => false,
    };
    const store = new Vuex.Store({ getters });
    const wrapper = shallowMount(Balance, { store, localVue });

    const titleHeader = wrapper.find('h3');
    expect(titleHeader.text()).toBe('Saldo atual');

    const balanceText = wrapper.find('h1');
    expect(balanceText.text()).toBe('R$ 0,00');
  });

  it('renders balance float', () => {
    const getters = {
      balancePrice: () => 1908.89,
      updateBalance: () => false,
    };
    const store = new Vuex.Store({ getters });
    const wrapper = shallowMount(Balance, { store, localVue });

    const balanceText = wrapper.find('h1');
    expect(balanceText.text()).toBe('R$ 1.908,89');
  });
});
