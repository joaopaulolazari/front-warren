import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import TransactionForm from '@/components/TransactionForm.vue';

describe('Components :: TransactionForm', () => {
  let localVue;
  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly layout of component', async () => {
    const wrapper = shallowMount(TransactionForm, {
      propsData: {
        title: 'depósito',
        type: 'deposit',
      },
    });

    const titleHeader = wrapper.find('h3');
    expect(titleHeader.text()).toBe('Realizar depósito');

    const buttons = wrapper.findAll({ name: 'v-btn' });
    expect(buttons.length).toBe(1);

    expect(wrapper.find('currency-input').exists()).toBeTruthy();
  });

  it('when fire form with value 0', async () => {
    const wrapper = shallowMount(TransactionForm, {
      propsData: {
        title: 'depósito',
        type: 'deposit',
      },
    });

    const btn = wrapper.find({ name: 'v-btn' });
    await btn.vm.$emit('click');

    expect(wrapper.emitted()['set-notification'][0][0].alert).toBeTruthy();
    expect(wrapper.emitted()['set-notification'][0][0].color).toBe('red');
    expect(wrapper.emitted()['set-notification'][0][0].message)
      .toBe('Valor da transação deve ser maior que R$ 0,00.');
  });

  it('when fire form with insuficient balance', async () => {
    global.fetch = jest.fn(() => Promise.resolve({ status: 422 }));

    const wrapper = shallowMount(TransactionForm, {
      propsData: {
        title: 'depósito',
        type: 'deposit',
      },
    });
    wrapper.setData({ value: 5000 });

    const btn = wrapper.find({ name: 'v-btn' });
    await btn.vm.$emit('click');

    expect(wrapper.emitted()['set-notification'][0][0].alert).toBeTruthy();
    expect(wrapper.emitted()['set-notification'][0][0].color).toBe('red');
    expect(wrapper.emitted()['set-notification'][0][0].message)
      .toBe('Saldo insuficiente.');
  });

  it('when fire form with other errors', async () => {
    global.fetch = jest.fn(() => Promise.resolve({ status: 400 }));

    const wrapper = shallowMount(TransactionForm, {
      propsData: {
        title: 'depósito',
        type: 'deposit',
      },
    });
    wrapper.setData({ value: 5000 });

    const btn = wrapper.find({ name: 'v-btn' });
    await btn.vm.$emit('click');

    expect(wrapper.emitted()['set-notification'][0][0].alert).toBeTruthy();
    expect(wrapper.emitted()['set-notification'][0][0].color).toBe('yellow');
    expect(wrapper.emitted()['set-notification'][0][0].message)
      .toBe('Indisponibilidade de transação, tente novamente mais tarde.');
  });

  it('when fire form with success', async () => {
    global.fetch = jest.fn(() => Promise.resolve({ status: 201 }));

    const wrapper = shallowMount(TransactionForm, {
      propsData: {
        title: 'depósito',
        type: 'deposit',
      },
    });
    wrapper.setData({ value: 5000 });

    const btn = wrapper.find({ name: 'v-btn' });
    await btn.vm.$emit('click');

    expect(wrapper.emitted()['set-notification'][0][0].alert).toBeTruthy();
    expect(wrapper.emitted()['set-notification'][0][0].color).toBe('green');
    expect(wrapper.emitted()['set-notification'][0][0].message)
      .toBe('Transação efetuada com sucesso.');
  });
});
