import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import AppFooter from '@/components/AppFooter.vue';

describe('Components :: AppFooter', () => {
  let localVue;
  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly buttons', () => {
    const wrapper = shallowMount(AppFooter, {
      localVue,
    });
    const buttons = wrapper.findAll({ name: 'v-btn' });
    expect(buttons.exists()).toBeTruthy();
    expect(buttons.length).toBe(5);
  });

  it('renders correctly menu', () => {
    const wrapper = shallowMount(AppFooter, {
      localVue,
    });
    expect(wrapper.text()).toMatch('Home');
    expect(wrapper.text()).toMatch('Depósito');
    expect(wrapper.text()).toMatch('Pag');
    expect(wrapper.text()).toMatch('Resgate');
    expect(wrapper.text()).toMatch('Extrato');
  });
});
