import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import AppHeader from '@/components/AppHeader.vue';

describe('Components :: AppHeader', () => {
  let localVue;
  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly the header', () => {
    const wrapper = shallowMount(AppHeader);
    expect(wrapper.text()).toBe('Teste Warren');
  });
});
