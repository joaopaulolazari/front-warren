import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import Alert from '@/components/Alert.vue';

describe('Components :: Alert', () => {
  let localVue;
  beforeEach(() => {
    Vue.use(Vuetify);
    localVue = createLocalVue();
    localVue.use(Vuetify);
  });

  it('renders correctly the header', () => {
    const wrapper = shallowMount(Alert, {
      propsData: {
        notification: {
          alert: true,
          timeout: 50000,
          color: 'green',
          message: 'Mensagem da notificação.',
        },
      },
    });

    const snackBar = wrapper.find({ name: 'v-snackbar' });
    expect(snackBar.attributes().color).toBe('green');
    expect(snackBar.attributes().timeout).toBe('50000');
    expect(snackBar.attributes().value).toBeTruthy();
    expect(wrapper.text()).toBe('Mensagem da notificação.');
  });

  it('when dont show the alert', () => {
    const wrapper = shallowMount(Alert, {
      propsData: {
        notification: {
          alert: false,
          timeout: 50000,
          color: 'green',
          message: 'Mensagem da notificação.',
        },
      },
    });

    const snackBar = wrapper.find({ name: 'v-snackbar' });
    expect(snackBar.attributes().color).toBe('green');
    expect(snackBar.attributes().timeout).toBe('50000');
    expect(snackBar.attributes().value).toBeFalsy();
  });
});
