import Vue from 'vue';
import VueCurrencyInput from 'vue-currency-input';

import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import './plugins/filters';
import { store } from './store/store';

Vue.config.productionTip = false;
Vue.prototype.$apiUrl = 'http://localhost:3000/api';
Vue.prototype.$authorization = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiODA2ZjI3NDAtOGQ4My00MDQzLWFhNTUtYTc2NjA5NDZmODE0In0.QFBUtHEFSyzC9abiiEjueaReGWeo_b7qCmKP2QUfaGY';

Vue.use(VueCurrencyInput, {
  globalOptions: {
    locale: 'pt-br',
    currency: 'BRL',
    valueAsInteger: false,
    distractionFree: true,
    precision: 2,
    autoDecimalMode: true,
    valueRange: { min: 0 },
    allowNegative: false,
  },
});

new Vue({
  router,
  vuetify,
  store,
  render: (h) => h(App),
}).$mount('#app');
