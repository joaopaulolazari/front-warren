import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// eslint-disable-next-line import/prefer-default-export
export const store = new Vuex.Store({
  state: {
    updateBalance: true,
    balance: 0,
  },
  mutations: {
    changeUpdateBalance(state) {
      state.updateBalance = !state.updateBalance;
    },
    changeBalance(state, value) {
      state.balance = value;
    },
  },
  getters: {
    updateBalance: (state) => state.updateBalance,
    balancePrice: (state) => state.balance,
  },
});
