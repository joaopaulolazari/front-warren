import Vue from 'vue';

Vue.filter('formatPrice', (value) => {
  if (value !== 0 && !value) return '';
  const formated = (value / 1).toFixed(2).replace('.', ',');
  return formated.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
});

Vue.filter('formatDate', (value) => {
  if (!value) return '';
  const data = new Date(value);
  return `${data.getDate()}/${data.getMonth()}/${data.getFullYear()} ${data.getHours()}:${data.getMinutes()}`;
});

Vue.filter('paymentTypeToString', (value) => {
  if (!value) return '';
  switch (value) {
    case 'payment':
      return 'Pagamento';
    case 'deposit':
      return 'Depósito';
    default:
      return 'Resgate';
  }
});
