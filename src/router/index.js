import Vue from 'vue';
import VueRouter from 'vue-router';
import Balance from '../views/Balance.vue';
import Deposit from '../views/Deposit.vue';
import Payment from '../views/Payment.vue';
import Discharge from '../views/Discharge.vue';
import Extract from '../views/Extract.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Balance,
  },
  {
    path: '/deposit',
    name: 'Deposit',
    component: Deposit,
  },
  {
    path: '/payment',
    name: 'Payment',
    component: Payment,
  },
  {
    path: '/discharge',
    name: 'Discharge',
    component: Discharge,
  },
  {
    path: '/extract',
    name: 'Extract',
    component: Extract,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
